from requests import Session
from requests.auth import HTTPBasicAuth

from zeep import Client
from zeep.wsse.signature import Signature
from zeep.wsse.username import UsernameToken
from zeep.transports import Transport

wsdl_url = 'https://demo-servicesesb.datacredito.com.co/wss/idws2/services/ServicioIdentificacionPlus?wsdl'
certify = 'certificado.crt'

def method1():
    client = Client(wsdl_url)
    # result = client.service.ConvertSpeed(
    #     100, 'kilometersPerhour', 'milesPerhour')

    # assert result == 62.137

def method2():
    session = Session()
    session.verify = certify 
    username = 'direcciontecnologia@afiansa.com'
    password = 'Pw29022020/'
    session.auth = HTTPBasicAuth(username, password)
    transport_with_basic_auth = Transport(session=session)

    client = Client(
        wsdl=wsdl_url,
        transport=transport_with_basic_auth
    )


if __name__ == "__main__":
     method2()


# lave llavero: Qubilo2019##

# Clave OKTA
# Usuario: direcciontecnologia@afiansa.com
# Password: Pw29022020/
# usuario signature: 2-900053370

# Endpoint
# https://demo-servicesesb.datacredito.com.co/wss/idws2/services/ServicioIdentificacionPlus

# WSDL
# https://demo-servicesesb.datacredito.com.co/wss/idws2/services/ServicioIdentificacionPlus?wsdl